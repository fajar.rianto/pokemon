package com.example.pokemon.models

data class Ability(
    val name:String,
    val url:String
)
