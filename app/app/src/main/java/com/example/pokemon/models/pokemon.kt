package com.example.pokemon.models

import com.google.gson.annotations.SerializedName

data class Pokemon(
    val abilities: List<PokemonAbility>,
    @SerializedName("name")
    val name: String,
    @SerializedName("sprites")
    val sprites: PokemonSpirites,
    val url: String,
)





