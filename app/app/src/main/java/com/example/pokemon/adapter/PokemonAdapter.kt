package com.example.pokemon.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.pokemon.R
import com.example.pokemon.models.Pokemon

class PokemonAdapter(
    onItemClick1: List<Any>,
    private val onItemClick: OnItemClickListener
) : ListAdapter<Pokemon, PokemonAdapter.PokemonViewHolder>(PokemonDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.pokemon_item, parent, false)
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val pokemon = getItem(position)
        holder.bind(pokemon)
    }

    inner class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.pokemon_name)
      //  private val imageView: ImageView = itemView.findViewById(R.id.pokemon_sprite)

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val pokemon = getItem(position)
                    onItemClick.onItemClick(pokemon)
                }
            }
        }

        fun bind(pokemon: Pokemon?) {
            nameTextView.text = pokemon?.name


//            val requestOptions = RequestOptions()
//                .placeholder(R.drawable.baseline_image_24) // Gambar placeholder jika gambar tidak tersedia
//                .error(R.drawable.baseline_image_24) // Gambar untuk menangani kesalahan saat memuat gambar
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//
//            Glide.with(itemView.context)
//                .load("${pokemon?.url}.png")
//                .apply(requestOptions)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imageView)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(pokemon: Pokemon)
    }
}

class PokemonDiffCallback : DiffUtil.ItemCallback<Pokemon>() {
    override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
        return oldItem == newItem
    }
}