package com.example.pokemon.repositories

import com.example.pokemon.api.ApiService
import com.example.pokemon.models.Pokemon
import com.example.pokemon.models.PokemonListResponse
import retrofit2.Response

class PokemonRepository (private  val apiService: ApiService) {

    // Mendapatkan daftar Pokemon dari API
    suspend fun getPokemonList(offset: Int, limit: Int): Response<PokemonListResponse> {
        return apiService.getPokemonList(offset, limit)
    }

    // Mendapatkan detail Pokemon berdasarkan ID atau nama
    suspend fun getPokemonDetail(idOrName: String): Response<Pokemon> {
        return apiService.getPokemonDetail(idOrName)
    }
}