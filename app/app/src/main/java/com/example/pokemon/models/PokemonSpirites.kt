package com.example.pokemon.models

import com.google.gson.annotations.SerializedName

data class PokemonSpirites(
    @SerializedName("frontDefault")
    val frontDefault: String? = null,
    @SerializedName("frontShiny")
    val frontShiny: String?,
    @SerializedName("frontFemale")
    val frontFemale: String?,
    @SerializedName("frontShinyFemale")
    val frontShinyFemale: String?,
    @SerializedName("backDefault")
    val backDefault: String?,
    @SerializedName("backShiny")
    val backShiny: String?,
    @SerializedName("backFemale")
    val backFemale: String?,
    @SerializedName("backShinyFemale")
    val backShinyFemale: String?
)


