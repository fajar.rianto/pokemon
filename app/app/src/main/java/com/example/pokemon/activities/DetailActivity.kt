package com.example.pokemon.activities

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.pokemon.R
import com.example.pokemon.models.Pokemon
import com.example.pokemon.viewmodels.DetailViewModel

//class DetailActivity : AppCompatActivity() {
//
//    private lateinit var detailViewModel: DetailViewModel
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_detail)
//
////        val pokemon = intent.getParcelableExtra<Pokemon>("pokemon")
//
////        if (pokemon != null) {
//            detailViewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
//
//            // Set judul aktivitas sesuai nama Pokemon
////            supportActionBar?.title = pokemon.name
//
//            // Tampilkan informasi Pokemon
//          //  nameTextView.text = pokemon.name
////            loadPokemonImage(pokemon)
//
//            // Implementasikan logika untuk menampilkan informasi tambahan dari Pokemon,
//            // seperti abilities atau data lainnya sesuai dengan kebutuhan Anda.
//        } else {
//            // Handle jika data Pokemon tidak ditemukan atau jika ada kesalahan.
//           // nameTextView.text = "Data Pokemon tidak valid"
//        }
//    }
//
////    private fun loadPokemonImage(pokemon: Pokemon) {
////        Glide.with(this)
////            .load(pokemon.sprites?.frontDefault)
////            .placeholder(R.drawable.charmander) // Gambar placeholder jika gambar tidak tersedia
////            .into(imageView)
////    }
//}