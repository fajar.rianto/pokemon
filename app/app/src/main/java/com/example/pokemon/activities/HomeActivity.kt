package com.example.pokemon.activities

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemon.R
import com.example.pokemon.adapter.PokemonAdapter
import com.example.pokemon.api.ApiService
import com.example.pokemon.api.RetrofitClient
import com.example.pokemon.models.Pokemon
import com.example.pokemon.viewmodels.HomeViewModel
import com.example.pokemon.viewmodels.HomeViewModelFactory

class HomeActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var adapter: PokemonAdapter
    private lateinit var viewModel: HomeViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        progressBar = ProgressBar(this)

        recyclerView = findViewById(R.id.pokemon_list)

        // Inisialisasi adapter dan RecyclerView
        adapter = PokemonAdapter(emptyList(), object : PokemonAdapter.OnItemClickListener {
            override fun onItemClick(pokemon: Pokemon) {
                // Implementasi tindakan ketika item Pokemon diklik
                // Misalnya, pindah ke halaman detail Pokemon
            }
        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Inisialisasi ViewModel
        val apiService: ApiService = RetrofitClient.create()
        viewModel = ViewModelProvider(this, HomeViewModelFactory(apiService))
            .get(HomeViewModel::class.java)

        // Amati perubahan pada LiveData dari ViewModel
        viewModel.pokemonList.observe(this) { pokemonList ->
            adapter.submitList(pokemonList)
        }

        viewModel.isLoading.observe(this) { isLoading ->
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        viewModel.error.observe(this) { error ->
            if (!error.isNullOrEmpty()) {
                Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
            }
        }

        // Ambil daftar Pokemon saat aktivitas dimulai
        viewModel.getPokemonList(0, 20)  // Ganti dengan offset dan limit yang sesuai
    }

}
