package com.example.pokemon.database

import androidx.room.RoomDatabase
import com.example.pokemon.database.DAO.PokemonDao

abstract class PokemonDatabase : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
}