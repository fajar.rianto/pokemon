package com.example.pokemon.util

import android.content.Context
import android.widget.Toast

object Utils {

    // Fungsi untuk menampilkan pesan toast
    fun showToast(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, message, duration).show()
    }

}