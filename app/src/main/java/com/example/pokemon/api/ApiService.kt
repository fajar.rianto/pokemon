package com.example.pokemon.api

import com.example.pokemon.models.Pokemon
import com.example.pokemon.models.PokemonListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("pokemon/")
    suspend fun getPokemonList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Response<PokemonListResponse>

    // Mendapatkan detail Pokemon berdasarkan ID atau nama
    @GET("pokemon/{idOrName}/")
    suspend fun getPokemonDetail(@Path("idOrName") idOrName: String): Response<Pokemon>
}