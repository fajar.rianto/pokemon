package com.example.pokemon.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.pokemon.R

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Simulasikan penundaan selama beberapa detik (misalnya, 3 detik)
        Handler().postDelayed({
            // Pindah ke HomeActivity atau activity utama Anda setelah Splash Screen selesai
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }, 3000) // 3000 milidetik (3 detik)
    }
}
