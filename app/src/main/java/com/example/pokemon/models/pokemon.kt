package com.example.pokemon.models

data class Pokemon(
    val abilities: List<PokemonAbility>,
    val name: String,
    val sprites: PokemonSpirites
)





