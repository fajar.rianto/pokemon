package com.example.pokemon.models

data class PokemonListItem(
    val name: String,
    val url: String

)
