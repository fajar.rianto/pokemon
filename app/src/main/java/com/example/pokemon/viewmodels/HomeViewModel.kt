package com.example.pokemon.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.viewpager2.widget.ViewPager2.OffscreenPageLimit
import com.example.pokemon.api.ApiService
import com.example.pokemon.models.Pokemon
import com.example.pokemon.models.PokemonListResponse
import com.example.pokemon.repositories.PokemonRepository
import kotlinx.coroutines.launch

class HomeViewModel(apiService: ApiService) : ViewModel() {

    private val repository: PokemonRepository = PokemonRepository(apiService)

    // LiveData untuk daftar Pokemon
    private val _pokemonList: MutableLiveData<List<Pokemon>> = MutableLiveData()
    val pokemonList: LiveData<List<Pokemon>> get() = _pokemonList

    // LiveData untuk menampilkan loading
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    // LiveData untuk menampilkan pesan kesalahan
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> get() = _error

    // Mendapatkan daftar Pokemon dari repository
    fun getPokemonList(offset: Int, limit: Int) {
        viewModelScope.launch {
            _isLoading.value = true
            try {
                val response = repository.getPokemonList(offset, limit)
                if (response.isSuccessful) {
                    val pokemonListResponse = response.body()
                    if (pokemonListResponse != null) {
                        _pokemonList.value = pokemonListResponse.results
                    } else {
                        _error.value = "Data Pokemon kosong atau tidak valid"
                    }
                } else {
                    _error.value = "Gagal mengambil data Pokemon"
                }
            } catch (e: Exception) {
                _error.value = "Terjadi kesalahan: ${e.message}"
            }
            _isLoading.value = false
        }
    }
}

