package com.example.pokemon.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemon.api.ApiService
import com.example.pokemon.models.Pokemon
import com.example.pokemon.repositories.PokemonRepository
import kotlinx.coroutines.launch

class DetailViewModel(apiService: ApiService) : ViewModel() {

    private val repository: PokemonRepository = PokemonRepository(apiService)

    // LiveData untuk detail Pokemon
    private val _pokemonDetail: MutableLiveData<Pokemon?> = MutableLiveData()
    val pokemonDetail: MutableLiveData<Pokemon?> get() = _pokemonDetail

    // LiveData untuk menampilkan loading
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    // LiveData untuk menampilkan pesan kesalahan
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> get() = _error

    // Mendapatkan detail Pokemon dari repository
    fun getPokemonDetail(idOrName: String) {
        viewModelScope.launch {
            _isLoading.value = true
            try {
                val response = repository.getPokemonDetail(idOrName)
                if (response.isSuccessful) {
                    val pokemon = response.body()
                    _pokemonDetail.value = pokemon
                } else {
                    _error.value = "Gagal mengambil data Pokemon"
                }
            } catch (e: Exception) {
                _error.value = "Terjadi kesalahan: ${e.message}"
            }
            _isLoading.value = false
        }
    }
}